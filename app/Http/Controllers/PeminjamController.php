<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Peminjam;
use App\Barang;
use Illuminate\Support\Facades\Input;


class PeminjamController extends Controller
{
    //
    public function index()
    {
      $pemin = Peminjam::all();
      $bar = Barang::all();
      // dd($pemin);
      return view('welcome', ['pemin' => $pemin]);
    }

    public function create()
    {
      # code...
      $bar = Barang::all();

      return view('pinjam/input', ['bar' => $bar]);
    }

    public function store(Request $req, Peminjam $pemin, Barang $x)
    {
      # code...
      $p = new $pemin;
      $p->nape = $req->nama;
      $p->kelas = $req->kelas;
      $p->barang = $req->bar;
      $p->type = $req->type;
      $p->jumba = $req->jumlah;
      $p->tgl = $req->tgl;
      $p->tgl_p = $req->tgl_p;
      $p->keterangan = $req->ket;

      $p->save();

      return redirect('/');
    }

    public function edit(Peminjam $pemin, $id)
    {
      # code...
      $pemin = Peminjam::findOrFail($id);
      return view('pinjam/edit', ['pemin' => $pemin]);
    }

    public function update(Request $req, $id)
    {
      # code...
      $p = Peminjam::findOrFail($id);
      $p->nape = $req->nama;
      $p->kelas = $req->kelas;
      $p->barang = $req->bar;
      $p->jumba = $req->jumlah;
      $p->tgl = $req->tgl;
      $p->tgl_p = $req->tgl_p;
      $p->keterangan = $req->ket;

      $p->save();

      return redirect ('/');
    }
}
