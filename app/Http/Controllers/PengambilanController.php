<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengambilan;
use App\Barang_hp;

class PengambilanController extends Controller
{
    public function create()
    {
      # code...
      $bar = Barang_hp::all();

      return view('ngambil/input', ['bar' => $bar]);

    }
    public function store(Request $req, Pengambilan $peng)
    {
      # code...
      $p = new $peng;
      $p->nape = $req->nama;
      $p->status = $req->status;
      $p->barang = $req->bar;
      $p->jumba = $req->jumlah;
      $p->type = $req->type;
      $p->tgl = $req->tgl;
      $p->keterangan = $req->ket;

      $p->save();

      return redirect('/');
    }

}
