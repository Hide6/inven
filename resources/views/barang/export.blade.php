<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Export</title>
  </head>
  <body>
    <table>
      <thead>
        <tr>
          <td>Nama Barang</td>
          <td>Jenis Barang</td>
          <td>Type Barang</td>
          <td>Kode Barang</td>
          <td>Jumlah Awal</td>
          <td>Jumlah Akhir</td>
        </tr>
      </thead>
      <tbody>
        @foreach($barang as $b)
        <tr>
          <td>{{$b->naba}}</td>
          <td>{{$b->jeba}}</td>
          <td>{{$b->tyba}}</td>
          <td>{{$b->koba}}</td>
          <td>{{$b->juwal}}</td>
          <td>{{$b->jukhir}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>
