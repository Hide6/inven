<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Laporan Stock Oknam</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.4/css/buttons.dataTables.min.css" />
    <style>
      table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
          margin-right: 30px;
      }

      td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 8px;
      }

      tr:nth-child(even) {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
      }
    </style>
  </head>
  <body>
    <center><h3>Daftar Opname Barang </h3>
    <h2>SMK Negeri 10 Jakarta Timur</h2></center>
    <table class="display" cellspacing="0">
      <thead>
      <tr>
        <th>
          No
        </th>
        <th>Nama Barang</th>
        <th>Merk Barang</th>
        <th>Kode Barang</th>
        <th>Tanggal Pembelian</th>
        <th>Harga Barang</th>
        <th>Jumlah Awal</th>
        <th>Jumlah Akhir</th>
        <th>
          Keterangan
        </th>
      </tr>
    </thead>
    <tbody>@php $i=1; @endphp
      @foreach($bar as $b)
      <tr>
        <td>
          {{ $i++ }}
        </td>
        <td>{{$b->naba}}</td>
        <td>{{$b->merk}}</td>
        <td>{{$b->koba}}</td>
        <td>{{$b->tanggal}}</td>
        <td>{{$b->harga}}</td>
        <td>{{$b->juwal}}</td>
        <td>{{$b->jukhir}}</td>
        <td>
          {{$b->ket}}
        </td>
      </tr>
      @endforeach
    </tbody>
    </table>
    <script src="{{ asset('/js/pdf.js')}}"></script>
  </body>
</html>
