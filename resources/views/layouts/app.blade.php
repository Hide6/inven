<!DOCTYPE html>

<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Inventory</title>

		<!-- <meta name="description" content="Static &amp; Dynamic Tables" /> -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="{{ url('/css/bootstrap.min.css')}}" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->
    <link rel="stylesheet" href="{{ url('/css/jquery-ui.custom.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/chosen.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/bootstrap-datepicker3.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/bootstrap-timepicker.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/daterangepicker.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/bootstrap-datetimepicker.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/bootstrap-colorpicker.min.css')}}" />

		<!-- text fonts -->
		<link rel="stylesheet" href="{{ url('/css/fonts.googleapis.com.css')}}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{ url('/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="{{ url('/css/ace-skins.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/ace-rtl.min.css')}}" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="{{ url('/js/jquery-2.1.4.min.js')}}"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{ url('/js/jquery.mobile.custom.min.js')}}'>"+"<"+"/script>");
		</script>
		<script src="{{ url('/js/bootstrap.min.js')}}"></script>

		<!-- page specific plugin scripts -->
		<script src="{{ url('/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{ url('/js/jquery.dataTables.bootstrap.min.js')}}"></script>
		<script src="{{ url('/js/dataTables.buttons.min.js')}}"></script>
		<script src="{{ url('/js/buttons.flash.min.js')}}"></script>
		<script src="{{ url('/js/buttons.html5.min.js')}}"></script>
		<script src="{{ url('/js/buttons.print.min.js')}}"></script>
		<script src="{{ url('/js/buttons.colVis.min.js')}}"></script>
		<script src="{{ url('/js/dataTables.select.min.js')}}"></script>
    <script src="{{ url('/js/jquery-ui.custom.min.js')}}"></script>
		<script src="{{ url('/js/jquery.ui.touch-punch.min.js')}}"></script>
		<script src="{{ url('/js/chosen.jquery.min.js')}}"></script>
		<script src="{{ url('/js/spinbox.min.js')}}"></script>
		<script src="{{ url('/js/bootstrap-datepicker.min.js')}}"></script>
		<script src="{{ url('/js/bootstrap-timepicker.min.js')}}"></script>
		<script src="{{ url('/js/moment.min.js')}}"></script>
		<script src="{{ url('/js/daterangepicker.min.js')}}"></script>
		<script src="{{ url('/js/bootstrap-datetimepicker.min.js')}}"></script>
		<script src="{{ url('/js/bootstrap-colorpicker.min.js')}}"></script>
		<script src="{{ url('/js/jquery.knob.min.js')}}"></script>
		<script src="{{ url('/js/autosize.min.js')}}"></script>
		<script src="{{ url('/js/jquery.inputlimiter.min.js')}}"></script>
		<script src="{{ url('/js/jquery.maskedinput.min.js')}}"></script>
		<script src="{{ url('/js/bootstrap-tag.min.js')}}"></script>

		<!-- ace scripts -->
		<script src="{{ url('/js/ace-elements.min.js')}}"></script>
		<script src="{{ url('/js/ace.min.js')}}"></script>
		<script src="{{ url('/js/ace-extra.min.js')}}"></script>

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="/js/html5shiv.min.js"></script>
		<script src="/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="{{ url('/')}}" class="navbar-brand">
						<small>
							<i class="fa fa-inbox"></i>
							Iventory Barang
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
					<ul class="nav navbar-nav">
						<li>
							<a href="{{ url('/logout') }}"
									 onclick="event.preventDefault();
									 document.getElementById('logout-form').submit();">
									 <i class="ace-icon fa fa-sign-out"></i>
										Logout
							</a>
							 <form id="logout-form"
											action="{{ url('/logout') }}"
									method="POST"
									style="display: none;">
															{{ csrf_field() }}
								</form>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>



				<ul class="nav nav-list">
					<li class="active">
						<a href="{{ url('/barang/index')}}">
							<i class="menu-icon fa 	fa-home"></i>
							<span class="menu-text"> Home </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="{{ url('/')}}">
							<i class="menu-icon fa 	fa-users"></i>
							<span class="menu-text"> Daftar Peminjam </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-inbox"></i>
							<span class="menu-text">
								Barang Pinjam
							</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">

							<li class="">
								<a href="{{ url('/barang/tambah')}}" role="button" class="blue" data-toggle="modal">
									<i class="menu-icon fa glyphicon-plus"></i>
									Tambah Barang
								</a>

								<b class="arrow"></b>
							</li>
							<li class="">
								<a href="{{ url('/barang/import')}}" role="button" class="blue" data-toggle="modal">
									<i class="menu-icon fa fa-cloud-upload"></i>
									Impor Data Barang
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-inbox"></i>
							<span class="menu-text">
								Barang Habis Pakai
							</span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">

							<li class="">
								<a href="{{ url('/barang_hp/tambah')}}" role="button" class="blue" data-toggle="modal">
									<i class="menu-icon fa glyphicon-plus"></i>
									Tambah Barang
								</a>

								<b class="arrow"></b>
							</li>
							<li class="">
								<a href="{{ url('/barang_hp/import')}}" role="button" class="blue" data-toggle="modal">
									<i class="menu-icon fa fa-cloud-upload"></i>
									Impor Data Barang
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>

			<div class="main-content">
				@yield('content')
			</div><!-- /.main-content -->

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<center>
						<span class="bigger-120">
							<span class="blue bolder">SMK Negeri 10 Jakarta</span>
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="/#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="/#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="/#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
					</center>
					</div>
				</div>
			</div>

			<a href="/#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->



	</body>
</html>
