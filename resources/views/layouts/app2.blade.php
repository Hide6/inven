<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>Iventory Barang</title>

		<meta name="description" content="top menu &amp; navigation" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<link rel="stylesheet" href="{{ url('/css/bootstrap.min.css')}}" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->
    <link rel="stylesheet" href="{{ url('/css/jquery-ui.custom.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/chosen.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/bootstrap-datepicker3.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/bootstrap-timepicker.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/daterangepicker.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/bootstrap-datetimepicker.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/bootstrap-colorpicker.min.css')}}" />

		<!-- text fonts -->
		<link rel="stylesheet" href="{{ url('/css/fonts.googleapis.com.css')}}" />

		<!-- ace styles -->
		<link rel="stylesheet" href="{{ url('/css/ace.min.css')}}" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/css/ace-part2.min.css" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="{{ url('/css/ace-skins.min.css')}}" />
		<link rel="stylesheet" href="{{ url('/css/ace-rtl.min.css')}}" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->
		<script src="{{ url('/js/jquery-2.1.4.min.js')}}"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='{{ url('/js/jquery.mobile.custom.min.js')}}'>"+"<"+"/script>");
		</script>
		<script src="{{ url('/js/bootstrap.min.js')}}"></script>

		<!-- page specific plugin scripts -->
		<script src="{{ url('/js/jquery.dataTables.min.js')}}"></script>
		<script src="{{ url('/js/jquery.dataTables.bootstrap.min.js')}}"></script>
		<script src="{{ url('/js/dataTables.buttons.min.js')}}"></script>
		<script src="{{ url('/js/buttons.flash.min.js')}}"></script>
		<script src="{{ url('/js/buttons.html5.min.js')}}"></script>
		<script src="{{ url('/js/buttons.print.min.js')}}"></script>
		<script src="{{ url('/js/buttons.colVis.min.js')}}"></script>
		<script src="{{ url('/js/dataTables.select.min.js')}}"></script>
		<script src="{{ url('/js/jquery-ui.custom.min.js')}}"></script>
		<script src="{{ url('/js/jquery.ui.touch-punch.min.js')}}"></script>
		<script src="{{ url('/js/chosen.jquery.min.js')}}"></script>
		<script src="{{ url('/js/spinbox.min.js')}}"></script>
		<script src="{{ url('/js/bootstrap-datepicker.min.js')}}"></script>
		<script src="{{ url('/js/bootstrap-timepicker.min.js')}}"></script>
		<script src="{{ url('/js/moment.min.js')}}"></script>
		<script src="{{ url('/js/daterangepicker.min.js')}}"></script>
		<script src="{{ url('/js/bootstrap-datetimepicker.min.js')}}"></script>
		<script src="{{ url('/js/bootstrap-colorpicker.min.js')}}"></script>
		<script src="{{ url('/js/jquery.knob.min.js')}}"></script>
		<script src="{{ url('/js/autosize.min.js')}}"></script>
		<script src="{{ url('/js/jquery.inputlimiter.min.js')}}"></script>
		<script src="{{ url('/js/jquery.maskedinput.min.js')}}"></script>
		<script src="{{ url('/js/bootstrap-tag.min.js')}}"></script>

		<!-- ace scripts -->
		<script src="{{ url('/js/ace-elements.min.js')}}"></script>
		<script src="{{ url('/js/ace.min.js')}}"></script>
		<script src="{{ url('/js/ace-extra.min.js')}}"></script>
		<!-- inline scripts related to this page -->



		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="/js/html5shiv.min.js"></script>
		<script src="/js/respond.min.js"></script>
		<![endif]-->
	</head>

	<body class="no-skin">
		<div id="navbar" class="navbar navbar-default    navbar-collapse       h-navbar ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="index.html" class="navbar-brand">
						<small>
							<i class="fa fa-dropbox"></i>
							Iventory Barang
						</small>
					</a>

					<button class="pull-right navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#sidebar">
						<span class="sr-only">Toggle sidebar</span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>

						<span class="icon-bar"></span>
					</button>
				</div>

				<div class="navbar-buttons navbar-header pull-right  collapse navbar-collapse" role="navigation">
					<ul class="nav navbar-nav">
							<li>
								<a href="{{ url('/login')}}">
									<i class="ace-icon fa fa-sign-in"></i>
									Masuk
								</a>
							</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<div id="sidebar" class="sidebar      h-sidebar                navbar-collapse collapse          ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<ul class="nav nav-list">
					<li class="active open hover">
						<a href="{{ url('/')}}">
							<i class="menu-icon fa fa-home"></i>
							<span class="menu-text"> Home </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="{{ url('/pinjam/input')}}">
							<i class="menu-icon fa fa-plus-circle"></i>
							<span class="menu-text"> Tambah Peminjam </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="{{ url('/ngambil/input')}}">
							<i class="menu-icon fa fa-plus-circle red"></i>
							<span class="menu-text"> Mengambil Barang </span>
						</a>

						<b class="arrow"></b>
					</li>
				</ul><!-- /.nav-list -->
			</div>

			<div class="main-content">
				@yield('content')
			</div><!-- /.main-content -->

			<div class="footer">
				<div class="footer-inner">
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">SMK Negeri 10 Jakarta</span>
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="https://www.facebook.com/SMK-Negeri-10-Jakarta-SMEAN-6-Jakarta-625551170914597/?fref=ts">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>
						</span>
					</div>
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<!-- ace settings handler -->

	</body>
</html>
