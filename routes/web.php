<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


// Route::get('login', 'Auth\Auth@admin');

Auth::routes();

// if(Auth::guest()) {
//   Route::get('/', ['as' => 'login', 'uses' => 'Auth\LoginController@not_login']);
// }
// else{
Route::get('/', function () {
    return view('welcome');
});
// }
// Route::get('/barang/input', 'BarangController@index');

// Route::get('/', function(){
//   return view('barang.index');
// });

Route::get('barang/index', 'BarangController@index');
Route::post('barang/tambah', 'BarangController@store');
Route::get('barang/ubah/{id}', 'BarangController@edit');
Route::post('barang/ubah/{id}', 'BarangController@update');
Route::get('barang/delete/{id}', 'BarangController@delete');
Route::get('barang/tambah', 'BarangController@create');
Route::get('barang/pdf', 'BarangController@pdf');
Route::get('barang/export', 'BarangController@export');
Route::post('barang/import', 'BarangController@import');
Route::get('barang/import', 'BarangController@upload');
Route::get('barang/detail/{naba}', 'BarangController@detail');
Route::get('barang/detail2/{merk}', 'BarangController@detail2');

Route::post('barang_hp/tambah', 'BHPController@store');
Route::get('barang_hp/ubah/{id}', 'BHPController@edit');
Route::post('barang_hp/ubah/{id}', 'BHPController@update');
Route::get('barang_hp/delete/{id}', 'BHPController@delete');
Route::get('barang_hp/tambah', 'BHPController@create');
Route::get('barang_hp/report', 'BHPController@pdf');
Route::get('barang_hp/pdf', 'BHPController@pdf');
Route::get('barang_hp/export', 'BHPController@export');
Route::get('barang_hp/export1', 'BHPController@export1');
Route::post('barang_hp/import', 'BHPController@import');
Route::get('barang_hp/import', 'BHPController@upload');
Route::get('barang_hp/detail/{naba}', 'BHPController@detail');

Route::get('pinjam/input', 'PeminjamController@create');
Route::post('pinjam/input', 'PeminjamController@store');
Route::get('/', 'PeminjamController@index');
Route::get('pinjam/edit/{id}', 'PeminjamController@edit');
Route::post('pinjam/edit/{id}', 'PeminjamController@update');

Route::get('ngambil/input', 'PengambilanController@create');
Route::post('ngambil/input', 'PengambilanController@store');
